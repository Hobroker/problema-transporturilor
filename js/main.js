var data = {
        a: [],
        b: [],
        c: []
    },
    exampleData = {
        a: [15, 20, 25],
        b: [12, 17, 19, 12],
        c: [
            [4, 3, 2, 2],
            [5, 6, 2, 4],
            [3, 2, 4, 3]
        ]
    },
    exampleData2 = {
        a: [12, 11, 17],
        b: [7, 8, 15, 10],
        c: [
            [5, 6, 4, 2],
            [3, 9, 2, 1],
            [7, 6, 10, 11]
        ]
    },
    exampleData3 = {
        a: [14, 19, 21],
        b: [11, 15, 18, 10],
        c: [
            [2, 6, 5, 4],
            [2, 3, 4, 5],
            [7, 6, 5, 4]
        ]
    },
    finalData = [],
    finalDataU = [],
    finalDataV = [];
$(document).ready(function() {
    $('input').addClass('form-control');
    $('.result').hide();
    $('.randomData li').on("click", function() {
        reset()
        randomData($(this).attr('class'));
    });
    $('.data').submit(function(e) {
        e.preventDefault()
        solveIt()
    });

    //automate
    randomData('exampleData3');
    $('.data').submit();
})

function solveIt() {
    getData();
    $('.alert.firstStep').hide();
    if (!isValid()) {
        $('.alert.firstStep').show();
        $('.alert-danger-proof').html(isNotValidProof())
        return;
    }
    $('.alert.firstStep').hide();

    solve('.t0');
    $('.result').show();
}

function solve(what) {
    $('.stuff .t-out').clone().addClass(what.substr(1)).appendTo($('.place-t0'))
    addTdClasses(what);

    $('.result .t-out' + what + ' .rowA').each(function(k) {
        last = $('.result .t-out' + what + ' .rowA' + (k + 1) + ' td:last');
        last.text(data.a[k])
        last.addClass('a' + (k + 1))
    });
    $('.result .t-out' + what + ' .rowB td').each(function(k) {
        $(this).text(data.b[k]);
        $(this).addClass('b' + (k + 1))
    });
    for (var i = 0; i < data.c.length; i++) {
        for (var j = 0; j < data.c[i].length; j++) {
            $('.result .t-out' + what + ' .c' + (i + 1) + (j + 1)).text(data.c[i][j])
        }
    }
    $(what + ' .a1b1').text(data.b[0])
    $(what + ' .a1b2').text(data.a[0] - parseInt($(what + ' .a1b1').text()))
    $(what + ' .a2b2').text(data.b[1] - parseInt($(what + ' .a1b2').text()))
    $(what + ' .a2b3').text(data.a[1] - parseInt($(what + ' .a2b2').text()))
    $(what + ' .a3b3').text(data.b[2] - parseInt($(what + ' .a2b3').text()))
    $(what + ' .a3b4').text(data.a[2] - parseInt($(what + ' .a3b3').text()))
    result = getZXx(what);
    $('.firstResultRezolvare').html('<small>' + result + '</small>');
    $('.firstResultResult').html('<strong><u>' + eval(result) + '</u></strong>')
    $('.p-result-1 .uus').empty();
    // testGauss();
    getUV();
    for (var i = 0; i < finalData.length; i++) {
        for (var j = 0; j < finalData[i].length; j++) {
            if (finalData[i][j] != 0) {
                text = '<p><strong>U<sub>' + (i + 1) + '</sub> + V<sub>' + (j + 1) + '</sub> = ' + data.c[i][j] + '</strong>, <span class="text-danger">';
                text += 'U<sub>' + (i + 1) + '</sub> = ' + finalDataU[i] + ', V<sub>' + (j + 1) + '</sub> = ' + finalDataV[j] + '</span></p>'
                $('.p-result-1 .busy-cells').append(text)
            }
        }
    }
    var step2 = [];
    for (var i = 0; i < finalData.length; i++) {
        for (var j = 0; j < finalData[i].length; j++) {
            if (finalData[i][j] == 0) {
                text = '&alpha;<sub>' + (i + 1) + ',' + (j + 1) + '</sub> = ';
                text += '(U<sub>' + (i + 1) + '</sub> + V<sub>' + (j + 1) + '</sub>) - C<sub>' + (i + 1) + (j + 1) + '</sub> = ';
                text += '(' + bn(finalDataU[i]) + ' + ' + bn(finalDataV[j]) + ') - ' + bn(data.c[i][j]) + ' = ';
                r = ((finalDataU[i] + finalDataV[j]) - data.c[i][j]);
                if (r < 0) {
                    step2.push(r);
                }
                text += '<strong>' + r + '</strong></p>';
                $('.p-result-1 .free-cells').append(text)
            }
        }
    }
    $('.free-cells2').text(step2.toString())
    $('.free-cells3').text(Math.min.apply(Math, step2))
    if (Math.min.apply(Math, step2) < 0) {
        $('.notDone0').show();
        $('.stuff .t-out').clone().addClass('t1').appendTo($('.place-t1'))
    } else {
        $('.t0Result').show();

    }
};

function bn(q) {
    return q < 0 ? '(' + q + ')' : q;
}

function getUV() {
    v = 0;
    u = 0;
    finalDataU[u++] = 0;
    for (var i = 0; i < finalData.length; i++) {
        for (var j = 0; j < finalData[i].length; j++) {
            if (finalData[i][j] != 0) {
                if (i == 0) {
                    finalDataV[v++] = (parseInt(data.c[i][j]) - parseInt(finalDataU[u - 1]));
                } else if (finalDataV[j] != undefined) {
                    finalDataU[u++] = (parseInt(data.c[i][j]) - parseInt(finalDataV[v - 1]));
                } else if (finalDataU[i] != undefined) {
                    finalDataV[v++] = (parseInt(data.c[i][j]) - parseInt(finalDataU[i]));
                }
                console.log('u' + (i + 1) + ' = ' + finalDataU[u - 1] + ', v' + (j + 1) + ' = ' + finalDataV[v - 1]);
            };
        }
    }
}

function testGauss() {
    $A = [
        [0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 1]
    ];
    $x = [4, 3, 6, 2, 4, 3];
    $result = gauss($A, $x);
    console.log($result.toString());
}

function addTdClasses(what) {
    var i = 0,
        j = 0;
    $('.result .t-out' + what + ' .rowA').each(function(k) {
        j = 0;
        $('.result .t-out' + what + ' .rowA' + (k + 1) + ' td').each(function(k) {
            if (k % 2 != 0) {
                $(this).addClass('a' + (i + 1) + 'b' + (j + 1));
                j++;
            } else if (k > 0) {
                $(this).addClass('c' + (i + 1) + '' + (j))
            }
        });
        i++;
    });
}

function isValid() {
    if (arraySum(data.a) != arraySum(data.b))
        return false;
    else
        return true;
}

function isNotValidProof() {
    return arraySum(data.a) + ' &ne; ' + arraySum(data.b);
}

function arraySum(a) {
    var sum = 0;
    a.forEach(function(element) {
        sum += element;
    });
    return sum;
}

function randomData(what) {
    data = window[what];
    var i = 0,
        j = 0;
    $('.input-table .rowA td').each(function() {
        $('input', this).val(data.a[i++])
    });
    i = 0;
    $('.input-table .rowB td').each(function() {
        $('input', this).val(data.b[i++])
    });
    i = 0;
    $('.input-table .rowC').each(function(k, obj) {
        j = 0;
        $('.input-table .rowC.rowC' + k + ' input').each(function() {
            $(this).val(data.c[i][j++])
        });
        i++;
    });
}

function getData() {
    var i = 0,
        j = 0;
    $('.input-table .rowA td').each(function() {
        data.a[i++] = parseInt($('input', this).val())
    });
    i = 0;
    $('.input-table .rowB td').each(function() {
        data.b[i++] = parseInt($('input', this).val())
    });
    i = 0;
    $('.input-table .rowC').each(function(k, obj) {
        j = 0;
        data.c[i] = [];
        $('.input-table .rowC.rowC' + k + ' input').each(function() {
            data.c[i][j++] = $(this).val();
        });
        i++;
    });
}

function getZXx(what) {
    var solving = [],
        result = 0;
    for (var i = 1; i <= 3; i++) {
        finalData[i - 1] = [];
        for (var j = 1; j <= 4; j++) {
            if ($('.a' + i + 'b' + j).text() != "") {
                $('.a' + i + 'b' + j).addClass('bg-info');
                $('.c' + i + j).addClass('bg-info');
                solving.push('(' + $('.a' + i + 'b' + j).text() + '*' + $('.c' + i + j).text() + ')');
                finalData[i - 1].push(parseInt($('.a' + i + 'b' + j).text()));
            } else {
                finalData[i - 1].push(0);
            }

        }
    }
    return solving.join(' + ');
}

function reset() {
    $('.result').hide();
    $('.alert.firstStep').hide();
}
